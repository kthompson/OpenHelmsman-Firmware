/* Mongoose Server - thanks to Mongoose and N. Kolban
*/
#include "webServer.h"

#include <string.h>
#include "stdint.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_system.h"
#include "esp_ota_ops.h"
#include "esp_partition.h"

#include "nvs_flash.h"

#include "lwip/sockets.h"
#include "lwip/netdb.h"

#include "wifi.h"
#include "assetFile.h"
#include "mongoose.h"

const static char *TAG = "webserver";

static int mongooseStrCmp(const char *str1, const struct mg_str *str2) {
	return strncmp(str1, str2->p, str2->len);
}
								
static void mongooseEventHandler(struct mg_connection *mgConn, int event, void *eventData){
	struct http_message *message = (struct http_message *) eventData;
	
	switch(event) {
	case MG_EV_HTTP_REQUEST:
		ESP_LOGI(TAG, "received HTTP request")
		
		ESP_LOGI(TAG, "Received URI: %.*s", message->uri.len, message->uri.p)
		const unsigned char *assetStart, *assetEnd, *mimeType;
		char contentType[50];
		strcpy(contentType, "Content-type: ");
		
		//TODO mbuff remove receive buffer
		
		if (getAsset(message->uri.p, &assetStart, &assetEnd, &mimeType, message->uri.len)) {
			ESP_LOGI(TAG, "Asset lookup successful.")
			const uint assetSize = assetEnd - assetStart;
			ESP_LOGI(TAG, "message response in %d bytes", assetSize)
			
			strcat(contentType, (char*) mimeType);
			strcat(contentType, "\nContent-Encoding: gzip");
			
			mg_send_head(mgConn, 200, assetSize, contentType);
			mg_send(mgConn, assetStart, assetSize);
		}
		
		else {
			mg_send_head(mgConn, 404, 0, "Content-Type: text/html");
		}

		break;
	}
}

static const esp_partition_t *updatePartition, *runningPartition;
static esp_ota_handle_t updateHandle;
static int imageLength;

static void uploadFirmware(struct mg_connection *mgConn, int event, void *eventData) {
	esp_err_t err;
	
	switch (event) {
	case MG_EV_HTTP_PART_BEGIN:
		runningPartition = esp_ota_get_running_partition();
		ESP_LOGI(TAG, "Firmware running on partition at 0x%08x", runningPartition->address);
		updatePartition = esp_ota_get_next_update_partition(NULL);
		ESP_LOGI(TAG, "Firmware being downloaded to partition at 0x%08x", updatePartition->address);
		
		imageLength = 0;
		updateHandle = 0;
		err = esp_ota_begin(updatePartition, OTA_SIZE_UNKNOWN, &updateHandle);
		if (err != ESP_OK) ESP_LOGE(TAG, "Failed to begin update. Error: 0x%x", err)
		break;
		
	case MG_EV_HTTP_PART_DATA: ;
		struct mg_http_multipart_part *multipart = (struct mg_http_multipart_part*) eventData;
		err = esp_ota_write(updateHandle, multipart->data.p, multipart->data.len);
		if (err != ESP_OK) ESP_LOGE(TAG, "Failed to write firmware chunk. Error: 0x%x", err)
		imageLength += multipart->data.len;
		ESP_LOGI(TAG, "Wrote %d for a total of %d", multipart->data.len, imageLength)
		break;
	
	case MG_EV_HTTP_PART_END:
		ESP_LOGI(TAG, "Firmware update finished. Wrote %d", imageLength)
		err = esp_ota_end(updateHandle);
		if (err != ESP_OK) ESP_LOGE(TAG, "Ending the OTA update process failed! Error: 0x%x", err);

		err = esp_ota_set_boot_partition(updatePartition);
		if (err != ESP_OK) ESP_LOGE(TAG, "Setting the new boot partition failed! Error: 0x%x", err);
		
		mg_send_head(mgConn, 200, 0, "Content-Type: text/html");
		break;
    }
}
								
static void serverTask()
{
	struct mg_mgr mgEvents;
	mg_mgr_init(&mgEvents, NULL);
	ESP_LOGI(TAG, "Web server started on port %s", HTTP_TCP_PORT)
	
	struct mg_connection *mgConn = mg_bind(&mgEvents, HTTP_TCP_PORT, mongooseEventHandler);
	mg_register_http_endpoint(mgConn, "/uploadfirmware", uploadFirmware);
	
	if (mgConn == NULL) {
		ESP_LOGI(TAG, "Failed to create connection")
		return;
	}
	
	mg_set_protocol_http_websocket(mgConn);
	
	//Keep polling the mongoose event handler
	while (1) {
		mg_mgr_poll(&mgEvents, 1000);
	}
}

void webServerInit(void) {
	/* Wait for the callback to set the CONNECTED_BIT in the wifi event group.*/
	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
	
	//this event bit gets set when the wifi fails. The thinking as that we may need to restart the entire task when the WIFI goes down
	xEventGroupClearBits(wifi_event_group, SERVER_RESTART);
	
	xTaskHandle webServerTask;
	xTaskCreate(serverTask, "Web Server Task", WEBSERVER_TASK_STACK_WORDS, NULL, SERVER_PRIORITY, &webServerTask);
}












