/* MDNS-SD Query and advertise Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "wifi.h"
#include "mdns.h"
#include "mdnsServer.h"

mdns_server_t * mdns = NULL;

static const char *TAG = "mdns";

void mdns_server()
{
	/* Wait for the callback to set the CONNECTED_BIT in the wifi event group.*/
	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);

	if (!mdns) {	//only initialise mdns server once
		esp_err_t err = mdns_init(TCPIP_ADAPTER_IF_STA, &mdns);
		if (err) {
			ESP_LOGE(TAG, "Failed starting MDNS: %u", err);
		}
		
		else {
			ESP_LOGI(TAG, "MDNS starting")
			
			ESP_ERROR_CHECK( mdns_set_hostname(mdns, HOSTNAME) );
			ESP_ERROR_CHECK( mdns_set_instance(mdns, HOSTDESCRIPTION));

			ESP_ERROR_CHECK( mdns_service_add(mdns, "_http", "_tcp", 80) );
			ESP_ERROR_CHECK( mdns_service_instance_set(mdns, "_http", "_tcp", "OpenHelmsman WebServer") );
			ESP_ERROR_CHECK( mdns_service_add(mdns, "_https", "_tcp", 443) );
			ESP_ERROR_CHECK( mdns_service_instance_set(mdns, "_https", "_tcp", "OpenHelmsman WebServer") );
		}
	}
}