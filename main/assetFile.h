/*This file is part of OpenHelmsman.

OpenHelmsman is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenHelmsman is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenHelmsman.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef ASSETFILE_H
#define ASSETFILE_H
 
unsigned char getAsset(const char *uri, const unsigned char **assetStart, const unsigned char **assetEnd, const unsigned char **mimeType, size_t strLen);

#endif