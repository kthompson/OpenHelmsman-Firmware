/* OpenSSL server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef OPENSSL_SERVER_H
#define OPENSSL_SERVER_H

#define WEBSERVER_TASK_NAME        "webServer"
#define WEBSERVER_TASK_STACK_WORDS 10240
#define WEBSERVER_TASK_PRIORITY    8

#define HTTP_TCP_PORT	"80"
#define CHUNK_SIZE		8096
#define SIMULTANEOUS_CONNS	8
#define SERVER_PRIORITY 1

//enable multipart HTTP for firmware updates

void webServerInit(void);

#endif