#
# Main component makefile.
#

COMPONENT_EMBED_FILES := dash/asset-manifest.json
COMPONENT_EMBED_FILES += dash/favicon.ico
COMPONENT_EMBED_FILES += dash/index.html
COMPONENT_EMBED_FILES += dash/manifest.json
COMPONENT_EMBED_FILES += dash/service-worker.js
COMPONENT_EMBED_FILES += dash/static/css/main.433ed380.css
COMPONENT_EMBED_FILES += dash/static/js/main.6196de06.js
COMPONENT_EMBED_FILES += dash/static/media/logo.5d5d9eef.svg
