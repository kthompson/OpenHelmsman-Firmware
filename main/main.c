#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "wifi.h"
#include "mdns.h"
#include "mdnsServer.h"
#include "webServer.h"

#define SSID "lodge"
#define PASSWORD "lodgelodge"

void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    initialise_wifi(SSID, PASSWORD);
	mdns_server();
	webServerInit();
}