/*
* Original Copyright (C) 2006-2016, ARM Limited, All Rights Reserved, Apache 2.0 License.
 * Additions Copyright (C) Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD, Apache 2.0 License.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#ifndef WIFI_H
#define WIFI_H
 
 void initialise_wifi(const char *ssid, const char *password);
 
 //pass a function that initialises a web server
 void bind_server_init(void (*server_init_ptr)(void));
 
/* FreeRTOS event group to signal wifi events */
EventGroupHandle_t wifi_event_group;

/* Are we connected to the AP with an IP? */
#define CONNECTED_BIT BIT0
#define SERVER_RESTART BIT1

#endif