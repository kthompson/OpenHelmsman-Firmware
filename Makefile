#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := OpenHelmsman

include $(IDF_PATH)/make/project.mk
CPPFLAGS += -D MG_ENABLE_HTTP_STREAMING_MULTIPART
